({
	doInit : function(component, event, helper) {
	},
    handleStartClick : function(component, event, helper) {
		helper.setStartTimeOnUI(component);
	},
    handleStopClick : function(component, event, helper) {
		helper.setStopTimeOnUI(component);
	},
    handleResetClick : function(component, event, helper) {
		helper.setResetTimeOnUI(component);
	}   
})